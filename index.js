const confLoad = require('./load')

module.exports = async app => {
  const engines = await confLoad(app.modules)
  const configs = Object.values(engines.configs)
  let context = Object.assign(app, engines)
  const runConfig = (run, config) => run.then(() => config(context))
  const queue = Promise.resolve()

  return configs
    .reduce(runConfig, queue)
    .then(() => context)
}
